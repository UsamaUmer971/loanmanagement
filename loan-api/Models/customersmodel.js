const mongoose=require('mongoose');
const customerSchema= mongoose.Schema({
    firstName:String,
    lastName:String,
    email:String,
    DOB:String,
    phoneNumber:Number
});
const customerModel=mongoose.model('Customers',customerSchema);
module.exports=customerModel; 