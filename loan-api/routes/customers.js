var express = require('express');
var router = express.Router();
var mongoose=require('mongoose')
const customerModel=require('../Models/customersmodel');
/* Get All Customers */
router.get('/list', function(req, res, next) {
  
  customerModel.find(function(err,customerResponseList){
    if(err){
      res.send({status:500,message:'Unable to find Customer'});
    }else{

      const recordCount= customerResponseList.length;
      res.send({status:200, recordCounts : recordCount, results :customerResponseList});
    }
    });
});
/* Get Specific Customers */
router.get('/view', function(req, res, next) {
  const userId = req.query.userId; 
  customerModel.findById(userId,function(err,customerList){
    if(err){
      res.send({status:500,message:'Unable to find Customer'});
    }else{

      const recordCount= customerList.length;
      res.send({status:200, recordCounts : recordCount, results :customerList});
    }
    });
});
/* Create new User. */
router.post('/add', function(req, res, next) {
  let customerObj = new customerModel({
    // firstName:'Umer',
    // lastName:'Usama',
    // email:"usama@gmail.com",
    // DOB:"22/7/1999",
    // phoneNumber:'7843848'
    firstName : req.body.firstName,
    lastName : req.body.lastName,
    email : req.body.email,
    DOB:req.body.DOB,
    phoneNumber:req.body.phoneNumber,
  });
  customerObj.save(function (err,customerObj){
    if(err){
      res.send({status:500,message:'Unable to add Customer'});
    }else{
      res.send({status:200,message:'Add Customer Successfully..!!', customerDetails:customerObj});
    }
  });
  
});
/* Update User. */
router.put('/update', function(req, res, next) {
  res.send('respond with a resource');
});
/* delete User. */
router.delete('/delete', function(req, res, next) {
  res.send('respond with a resource');
});
/* Search User. */
router.get('/search', function(req, res, next) {
  res.send('respond with a resource');
});
module.exports = router;
